<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Directory Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/directory_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Create a Directory Map
 *
 * Reads the specified directory and builds an array
 * representation of it.  Sub-folders contained with the
 * directory will be mapped as well.
 *
 * @access	public
 * @param	string	path to source
 * @param	int		depth of directories to traverse (0 = fully recursive, 1 = current dir, etc)
 * @return	array
 */
if ( ! function_exists('directory_map'))
{
	function directory_map($source_dir, $directory_depth = 0, $hidden = FALSE)
	{
		if ($fp = @opendir($source_dir))
		{
			$filedata	= array();
			$new_depth	= $directory_depth - 1;
			$source_dir	= rtrim($source_dir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

			while (FALSE !== ($file = readdir($fp)))
			{
				// Remove '.', '..', and hidden files [optional]
				if ( ! trim($file, '.') OR ($hidden == FALSE && $file[0] == '.'))
				{
					continue;
				}

				if (($directory_depth < 1 OR $new_depth > 0) && @is_dir($source_dir.$file))
				{
					$filedata[$file] = directory_map($source_dir.$file.DIRECTORY_SEPARATOR, $new_depth, $hidden);
				}
				else
				{
					$filedata[] = $file;
				}
			}

			closedir($fp);
			return $filedata;
		}

		return FALSE;
	}
}

if ( ! function_exists('directory_map_by_array'))
{
	function directory_map_by_array($map_dir, $directory_depth = 1, $hidden = FALSE)
	{
		//support directory_map
		if ( !is_array($map_dir))
		{
			return directory_map($map_dir, $directory_depth, $hidden);
		}

		$return_dir_map = array();

		foreach ($map_dir as $dir_index => $dir_value) 
		{
			//make a array to deal whit the config about the dir need to map
			$dir_map_config = array();
			if ( ! is_array($dir_value))
			{
				$return_dir_map[$dir_index] = directory_map($dir_value, $directory_depth, $hidden);
				continue;
			}
			//return the wrong message if the config wrong
			if ( ! isset($dir_value['dir']))
			{
				$return_dir_map[$dir_index] = 'error:You must set the dir.';
				continue;
			}
			else
			{
				$dir_map_config['dir'] = $dir_value['dir']; 
			}

			//support the default

			if ( ! isset($dir_value['depth']))
			{
				$dir_map_config['depth'] = $directory_depth;
			}
			else
			{
				$dir_map_config['depth'] = $dir_value['depth'];
			}

			if ( ! isset($dir_value['hidden']))
			{
				$dir_map_config['hidden'] = $hidden;
			}
			else
			{
				$dir_map_config['depth'] = $dir_value['hidden'];
			}

			//map the dir
			$return_dir_map[$dir_index] = directory_map($dir_map_config['dir'], $dir_map_config['depth'], $dir_map_config['hidden']);
		}

		return $return_dir_map;
	}
}

/* End of file directory_helper.php */
/* Location: ./system/helpers/directory_helper.php */