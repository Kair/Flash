/*
Navicat MySQL Data Transfer

Source Server         : Kair
Source Server Version : 50611
Source Host           : localhost:3306
Source Database       : flash

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2015-05-05 09:50:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for flash_ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `flash_ci_sessions`;
CREATE TABLE `flash_ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of flash_ci_sessions
-- ----------------------------
INSERT INTO `flash_ci_sessions` VALUES ('28fe37bb8f6c5c4b2a9b48dfb549850b', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', '1430755021', 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:8:\"password\";s:32:\"282f77cd12cb336ebb6e1f38840227de\";s:6:\"status\";s:1:\"7\";s:4:\"role\";s:5:\"admin\";}');
INSERT INTO `flash_ci_sessions` VALUES ('6ab8467be50580c0c64eea231ff4cc8f', '172.16.168.32', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.65 Safari/537.36', '1430748905', 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:8:\"password\";s:32:\"282f77cd12cb336ebb6e1f38840227de\";s:6:\"status\";s:1:\"7\";s:4:\"role\";s:5:\"admin\";}');
INSERT INTO `flash_ci_sessions` VALUES ('72dfa455a4bc0618e6b82f5332c8a63b', '172.16.212.147', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', '1430755048', 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:8:\"password\";s:32:\"282f77cd12cb336ebb6e1f38840227de\";s:6:\"status\";s:1:\"7\";s:4:\"role\";s:5:\"admin\";}');
INSERT INTO `flash_ci_sessions` VALUES ('a79ee72918c432f3a5ff4cc4398dba96', '172.16.168.32', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0', '1430748404', 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:4:\"test\";s:8:\"password\";s:32:\"282f77cd12cb336ebb6e1f38840227de\";s:6:\"status\";s:1:\"7\";s:4:\"role\";s:1:\"1\";}');
INSERT INTO `flash_ci_sessions` VALUES ('b5e6d9979dfacd2226e2a921d26c1a58', '172.16.111.13', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', '1430748390', '');
INSERT INTO `flash_ci_sessions` VALUES ('bd91c134814d5da71e8bbab157df2030', '172.16.111.13', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36 SE 2.X Meta', '1430755138', 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:4:\"test\";s:8:\"password\";s:32:\"282f77cd12cb336ebb6e1f38840227de\";s:6:\"status\";s:3:\"7,8\";s:4:\"role\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for flash_pro
-- ----------------------------
DROP TABLE IF EXISTS `flash_pro`;
CREATE TABLE `flash_pro` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '作品名',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `img` text COMMENT '截图',
  `intro` text COMMENT '简介',
  `flash` text COMMENT 'flash路径',
  `score` int(11) DEFAULT '0' COMMENT '分数',
  `score_intro` text COMMENT '评分细节',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of flash_pro
-- ----------------------------
INSERT INTO `flash_pro` VALUES ('7', '为了呼吸的旅途1', '陈晓敏', 'public/flash/img/f875ba122babda3098dc5793cd767ea3.jpg', '我他妈的改这句话就是为了测试而已', 'public/flash/flash/c197e3f4568004528bd176ddcf82c777.swf', '9', '{\"2\":{\"1\":\"10\",\"2\":\"7\",\"3\":\"10\"}}');
INSERT INTO `flash_pro` VALUES ('8', '为了呼吸的旅途', '陈晓敏', 'public/flash/img/5e010585070710014846f331610f3f6b.jpg', '测试一下给你看行不', 'public/flash/flash/cd4c188708d85149d8ac92a4741523ce.swf', '10', '{\"2\":{\"1\":\"10\",\"2\":\"10\",\"3\":\"10\"}}');

-- ----------------------------
-- Table structure for flash_type
-- ----------------------------
DROP TABLE IF EXISTS `flash_type`;
CREATE TABLE `flash_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '评分类别',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of flash_type
-- ----------------------------
INSERT INTO `flash_type` VALUES ('1', '纯洁度');
INSERT INTO `flash_type` VALUES ('2', '傻逼度');
INSERT INTO `flash_type` VALUES ('3', '神经毒');

-- ----------------------------
-- Table structure for flash_user
-- ----------------------------
DROP TABLE IF EXISTS `flash_user`;
CREATE TABLE `flash_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `status` varchar(255) DEFAULT '0' COMMENT '评分状态',
  `role` varchar(255) DEFAULT '1' COMMENT '角色',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of flash_user
-- ----------------------------
INSERT INTO `flash_user` VALUES ('1', 'CaFlash', '9ce5d6fd766d8dad2df88bca6464185b', '7', 'admin');
INSERT INTO `flash_user` VALUES ('2', 'test', '282f77cd12cb336ebb6e1f38840227de', '7,8', '1');
