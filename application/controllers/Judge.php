<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * Judge
 *
 * 裁判类
 *
 * @package     Controllers
 * @author      Kair
 */
class Judge extends CI_Controller 
{	

    /**
    * __construct
    * 控制器构造方法
    *
    * 用于检查权限
    */
	public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('role') != '1' && $this->router->fetch_method() != 'login')
        {
            redirect(base_url('Judge/login'), 'refresh');
            return;
        }
    }

    public function update_status()
    {
        $user = $this->session->userdata('user_id');
        $info = $this->admin_model->get_info_by_field($user,"user_id","user");
        $this->session->set_userdata('status',$info['status']);
    }


    /**
    * index
    * 加载登陆页
    *
    */
	public function index($id = FALSE)
	{ 
        //每次都做一个操作去更新session里面保存的裁判已经打分的作品
        $this->update_status();
        $data['flash'] = $this->admin_model->get_table('pro');
        $data['type']  = $this->admin_model->get_table('type');
        $data['dont_active'] = explode(',', $this->session->userdata('status'));
        if ($id === FALSE)
        {
            $data['active'] = $data['flash'][0];
        }
        else
        {
            foreach ($data['flash'] as $key => $item) 
            {
                if ($id == $item['pro_id'])
                {
                    $data['active'] = $item;
                }
            }
        }
		$this->load->view('Index/index',$data);
	}

    /**
    * index
    * 加载登陆页以及验证密码
    *
    */
	public function login()
	{
		if ($this->session->userdata('role') == '1')
		{
			redirect(base_url(''), 'refresh');
		}
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        if ($username && $password)
        {
            $userinfo = $this->admin_model->get_info_by_field($username,"username","user");
            if ($userinfo && $userinfo['password'] == md5(md5(base64_decode(md5($password)))) && $userinfo['role']=="1")
            {
                $this->session->set_userdata($userinfo);
                redirect(base_url(''), 'refresh');
                return;
            }
            else
            {
                echo iconv("UTF-8","GBK","<script>alert('用户名或密码错误，请返回检查。'); history.go(-1);</script>");
                return;
            }
        }
        $this->load->view('Index/login');
	}

    /**
    * save_score
    * 保存分数
    *
    */
    public function save_score()
    {
        $id     = $this->input->post('pro', TRUE);
        $pro    = $this->admin_model->get_info_by_field($id,'pro_id','pro');
        $type   = $this->admin_model->get_table('type');
        $user   = $this->session->userdata('user_id');
        
        //如果存在分数的话，那么就解析出来
        if ( ! empty($pro['score_intro']))
        {
            $score_info = json_decode($pro['score_intro'],TRUE);
        }
        else
        {
            $score_info = array();
        }
        //判断用户是否已经给过分
        if ( ! isset($score_info[$user]))
        {
            $score_info[$user] = array();
        }
        else
        {
            echo iconv("UTF-8","GBK","<script>alert('您已经评审过此作品!'); history.go(-1);</script>");
            return;
        }
        
        //计算平均分
        $score_all = 0;
        foreach ($type as $key => $value) 
        {
            $score_info[$user][$value['type_id']] = $this->input->post('select_'.$value['type_id'], TRUE);
            $score_all += $score_info[$user][$value['type_id']];
        }

        //把评分细节解析成json写进数据库
        $data['score_intro'] = json_encode($score_info);
        $score = $score_all / count($type);
        $data['score'] = $pro['score'] + $score;
        $condition['pro_id'] = $id;

        $user_update['status'] = $this->session->userdata('status').','.$id;
        $user_condition['user_id'] = $user;       

        if ($this->admin_model->update_field('pro',$data,$condition) == 1)
        {
            $this->admin_model->update_field('user',$user_update,$user_condition);
            $this->session->set_userdata('status',$user_update['status']);
            redirect(base_url(''), 'refresh');
        }
        else
        {
            echo iconv("UTF-8","GBK","<script>alert('异常!'); history.go(-1);</script>");
            return;
        }
    }
}