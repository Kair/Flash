<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * FlashAdmin
 *
 * 管理员控制器
 *
 * @package     Controllers
 * @author      Kair
 */
class FlashAdmin extends CI_Controller 
{

	/**
    * __construct
    * 控制器构造方法
    *
    * 用于检查权限
    */
	public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('role')!='admin' && $this->router->fetch_method() != 'index')
        {
            redirect(base_url('FlashAdmin'), 'refresh');
            return;
        }
    }

    /**
    * index
    * 加载首页以及验证密码
    *
    */
	public function index()
	{
		if ($this->session->userdata('role')=='admin')
		{
			redirect(base_url('FlashAdmin/view/index'), 'refresh');
		}
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        if ($username && $password)
        {
            $userinfo = $this->admin_model->get_info_by_field($username,"username","user");
            if ($userinfo && $userinfo['password'] == md5(md5(base64_decode(md5($password)))) && $userinfo['role']=="admin")
            {
                $this->session->set_userdata($userinfo);
                redirect(base_url('FlashAdmin/view/index'), 'refresh');
                return;
            }
            else
            {
                echo iconv("UTF-8","GBK","<script>alert('用户名或密码错误，请返回检查。'); history.go(-1);</script>");
                return;
            }
        }
        $this->load->view('admin/login');
	}

	/**
    * logout
    * 登出
    *
    */
	public function logout()
    {
        $this->session->sess_destroy(); 
        redirect(base_url('FlashAdmin/'), 'refresh');
    }

    /**
    * view
    * 加载视图方法
    *
    * @param    $view     string    需要加载的视图
    * @param    $condition string or null   需要附加的条件
    */
	public function view($view,$condition=null)
	{
		if ($view == "list")
		{
			//这里由于加载的视图可能会因为需要的信息不同，所以附加了一个$condition，来规定表名
			$data['table'] = $condition;
			if ($condition == "user")
			{
				$data['datas'] = $this->admin_model->get_table($condition,array('role'=>1));
				$data['field'] = "username";
				$this->load->view('admin/list',$data);
				return;
			}
			else
			{
				$data['datas'] = $this->admin_model->get_table($condition);
				$data['field'] = "name";

				$this->load->view('admin/list',$data);
				return;
			}
		}
		//如果不加载list，那么我就会直接加载admin下，你需要的视图
		if ($condition == null)
		{
			$this->load->view('admin/'.$view);
			return ;
		}
		$data['message'] = $condition;
		$this->load->view('admin/'.$view,$data);
	}

	/**
    * save
    * 信息的保存
    *
    * @param    $view     string    需要加载的视图
    * @param    $condition string or null   需要附加的条件
    */
	public function save($view,$condition=null)
	{
		$data = array();
		$table = "user";
		//如果condition是user的话，说明我是要增加user信息的，那么就会进行密码的处理
		if ($condition == 'user')
		{
			$data['username'] = $this->input->post("username", TRUE);
			$data['password'] = md5(md5(base64_decode(md5($this->input->post("username",TRUE)))));
		}
		else
		{
			$data['name'] = $this->input->post("name", TRUE);
			$table = "type";
		}
		$this->admin_model->add_info($data,$table);
		echo iconv("UTF-8","GBK","<script>alert('增加成功！'); </script>");
		redirect(base_url('FlashAdmin/view/'.$view), 'refresh');
	}

	/**
    * del_info
    * 删除信息
    *
    * @param    $table     string    表名
    * @param    $id 	   int 		 需要删除的id
    */
	public function del_info($table,$id)
	{
		if ($this->admin_model->del_info($table,array($table.'_id' => $id)))
		{
			echo iconv("UTF-8","GBK","<script>alert('删除成功！'); </script>");
			redirect(base_url('FlashAdmin/view/list/'.$table), 'refresh');
		}
		else
		{
			echo iconv("UTF-8","GBK","<script>alert('删除失败！'); </script>");
			redirect(base_url('FlashAdmin/view/list/'.$table), 'refresh');
		}
	}

	/**
    * save_pro
    * 保存作品信息
    *
    */
	public function save_pro()
	{
		$data['name'] 	= $this->input->post('name', TRUE);

		if ($this->admin_model->is_exist('pro',$data))
		{
			$data['author'] = $this->input->post('author', TRUE);
			$data['intro']	= $this->input->post('intro', TRUE);
			$this->session->set_userdata($data);
			redirect(base_url('FlashAdmin/view/loading/exist'), 'refresh');
			return ;
		}
		$data['author'] = $this->input->post('author', TRUE);
		$data['intro']	= $this->input->post('intro', TRUE);
		$this->load->library('upload');
		$img 			= $this->do_upload('img','img','png|gif|jpg');	//特别注意这里需要做东西的上传
		$flash 			= $this->do_upload('flash','flash','swf');
		$data['img'] 	= 'public/flash/img/'.$img['file_name'];
		$data['flash'] 	= 'public/flash/flash/'.$flash['file_name'];
		$this->admin_model->add_info($data,'pro');
		redirect(base_url('FlashAdmin/view/loading/success'), 'refresh');
	}

	/**
    * do_upload
    * 上传操作
    *
    * @param    $file     string    文件名
    * @param    $path 	  string 	保存路径
    * @param    $type     string    上传的类型
    */
	public function do_upload($file,$path,$type)
	{
		$config['upload_path'] 		= './public/flash/'.$path.'/';
		$config['allowed_types'] 	= $type;
		$config['max_width'] 	 	= '1024';
		$config['max_height']  		= '768';
		$config['encrypt_name'] 	= TRUE;
		
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($file))
		{
			redirect(base_url('FlashAdmin/view/loading/fail', 'refresh'));
		} 
		else
		{
			return $this->upload->data();
		}
	}

	/**
    * top
    * 加载排行榜视图而已
    *
    */
	public function top()
	{
		$data['top'] = $this->admin_model->get_top();
		foreach ($data['top'] as $key => $value) 
		{
			$score_detail = $this->get_score_detail($value);
			$data['top'][$key]['score'] = $score_detail['score'];
			$data['top'][$key]['judgeNum'] = $score_detail['judgeNum'];
		}
		$data['judgeAllNum'] = count($this->admin_model->get_table('user',array('role' => '1')));
		$this->load->view('Admin/top',$data);
	}

	/**
    * get_score_detail
    * 获得评分的结果还有裁判的人数
    * @param      $pro    string    作品id
    */
	public function get_score_detail($pro)
	{
		if ( ! empty($pro['score_intro']))
        {
            $score_info = json_decode($pro['score_intro'],TRUE);
        }
        else
        {
            $score_info = array();
        }
        $result['score'] = count($score_info)==0?0:$pro['score']/count($score_info);
        $result['judgeNum'] = count($score_info);
        return $result;

	}
}