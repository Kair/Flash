<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>FLASH评分系统</title>
    <link href="<?= base_url('public/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('public/css/admin-login.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('public/css/admin-style.css') ?>" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <form class="form-signin well" method="post" onsubmit="javascript: return addCheck()" >
            <fieldset>
                <legend >
                    <small>FLASH评分系统</small>
                </legend>
                <h2 class="form-signin-heading">登录</h2>
                <input type="text" class="input-big form-control" placeholder="用户名" name="username" id="username" required="required"><br>
                <input type="password" class="input-big form-control" placeholder="密码" name="password" id="password" required="required"><br>
                <button class="btn btn-primary" type="submit" id="submit">登录</button>
            </fieldset>
        </form>
    </div>
</div>
</body>