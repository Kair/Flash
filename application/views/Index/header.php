<!DOCTYPE html >
<html>

<head>
	<title>Minimalistic Web Template</title>
	<meta charset="utf-8">
	<link href="<?= base_url('public/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('public/css/style.css')?>" rel="stylesheet" type="text/css" />
	<!--[if IE 6]>
		<link href="<?=base_url('public/css/ie6.css')?>" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!--[if IE 7]>
        <link href="<?=base_url('public/css/ie7.css')?>" rel="stylesheet" type="text/css" />  
	<![endif]-->
	<style>
	*{
		box-sizing:content-box !important;
	}
	</style>
</head>

<body>
	  <div id="background">
			  <div id="page">
					<div class="header">
						<div class="footer">
							<div class="body">		  
									<div id="sidebar">
									    <a href=""><img id="logo" src="<?=base_url('public/images/logo.gif')?>" with="154" height="74" alt="" title=""/></a>
									
										
										<ul class="navigation">
										<?php if (isset($flash)):?>
											<?php foreach ($flash as $item): ?>
												<li <?= $active['pro_id'] == $item['pro_id']? "class='active'":'' ?>><a href="<?=base_url('Judge/index/'.$item['pro_id'])?>"><?=$item['name']?></a></li>
											<?php endforeach;?>
										<?php endif;?>
										</ul>
										
										
										<div class="footenote">
										  <span>&copy; Copyright &copy; 2015.</span>
										  <span><a href="http://ca.sise.com.cn">计算机协会</a>CA工作室 all rights reserved</span>
										</div>
										
									</div>