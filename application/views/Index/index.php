<?php $this->load->view('Index/header');?>
<script>
	function check(){
		if (confirm("确定提交吗？")){
			return true;
		}
		return false;
	}
</script>
<div id="content">
	<div class="container <?=in_array($active['pro_id'], $dont_active)? 'center-block':''?>">
		<div <?=in_array($active['pro_id'], $dont_active)? 'class="container"':''?>>
    	<embed class="matching" src="<?= base_url($active['flash'])?>">
    	</div>
    	<div class="row <?=in_array($active['pro_id'], $dont_active)? 'center-block':''?>">
    		<?php if (in_array($active['pro_id'], $dont_active)):?>
    		<div class="col-xs-12">
				<div class="well">
					<h3><?= $active['name']?><small>作者：<?= $active['author']?></small></h3>
					<br>
					<img src="<?= base_url($active['img'])?>" style="width:100%;" alt="" title="">
					<br>
					<blockquote>
					  <p><?= $active['intro']?></p>
					</blockquote>

				</div>
			</div>
    		<?php else:?>
    		<div class="col-xs-8">
				<div class="well">
					<h3><?= $active['name']?><small>作者：<?= $active['author']?></small></h3>
					<br>
					<img src="<?= base_url($active['img'])?>" style="width:100%;" alt="" title="">
					<br>
					<blockquote>
					  <p><?= $active['intro']?></p>
					</blockquote>

				</div>
			</div>
			<div class="col-xs-3">
				<div class="well">
				<?php if (isset($type)):?>
					<form action="<?= base_url('Judge/save_score')?>" class="form-inline" method='post' onsubmit="javascript: return check()">
					<?php foreach($type as $item):?>
							<div class="form-group block">
							    <label><?=$item['name'];?></label>
							      	<select name="select_<?=$item['type_id']?>" class="form-control input-sm">
							      		<?php for ($i = 10; $i >0; $i--):?>
							      			<option value="<?=$i?>"><?= $i?> 分</option>
							      		<?php endfor;?>
							      	</select>
							</div>
					<?php endforeach;?>	
						<input type="hidden" value='<?=$active['pro_id']?>' name='pro'>
						<input type="submit" class='sub-btn btn btn-info btn-block' value="提交评分">
					</form>
				<?php else:?>
					<div class="alert alert-danger" role="alert">未设置评分项</div>
				<?php endif;?>
				</div>
			</div>
			<?php endif;?>
		</div>
	</div>

</div>

<?php $this->load->view('Index/footer');?>