<?php $this->load->view('Admin/header')?>
<div class="container">
	<a class="btn btn-primary" href="<?= base_url('FlashAdmin/top')?>">刷新</a>
	<br>
	<br>
	<table class="table table-bordered">
    <thead>
	    <tr>
	        <th>名次</th>
	        <th>作品名</th>
	        <th>作者</th>
	        <th>得分</th>
	        <th>已评分评委人数</th>
	    </tr>
    </thead>
    <tbody>
    <?php 
    $count = 1;
    foreach($top as $item):
    ?>
    	<tr>
    		<td><?= $count++;?></td>
    		<td><?= $item['name']?></td>
    		<td><?= $item['author']?></td>
    		<td><?= $item['score']?></td>
    		<td><?= $item['judgeNum']?>/<?= $judgeAllNum?></td>
    	</tr>
    <?php endforeach;?>
    </tbody>
</table>

</div>

<?php $this->load->view('Admin/footer')?>