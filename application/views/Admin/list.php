<?php $this->load->view('Admin/header')?>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <?php
            switch ($table) {
              case 'user':
                echo "所有用户";
                break;
              case 'pro':
                echo "所有作品";
                break;
              case 'type':
                echo "所有评分标准";
                break;
            }
          ?>
        </div>

        <div class="panel-body">

        <a href="<?=base_url('FlashAdmin/view/add_'.$table)?>" class="btn btn-lg btn-primary">新增</a>

          <?php foreach ($datas as $data):?>
            <hr>
            <div class="page">
              <h4><?=$data[$field]?></h4>
            </div>
            <form action="<?=base_url('FlashAdmin/del_info/'.$table.'/'.$data[$table.'_id'])?>" method="POST" style="display: inline;">
              <button type="submit" class="btn btn-danger">删除</button>
            </form>
          <?php endforeach;?>

        </div>

      </div>
    </div>
  </div>
</div>
<?php $this->load->view('Admin/footer')?>