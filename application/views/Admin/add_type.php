<?php $this->load->view('Admin/header')?>
<script>
$(document).ready(function () {
    $("#name").focus();
});
</script>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">新增评分标准</div>

        <div class="panel-body">

          <form action="<?=base_url('FlashAdmin/save/add_type');?>" method="POST">
            <input type="text" id="name" name="name" class="form-control" placeholder="名称" required="required">
            <br>
            <button class="btn btn-lg btn-info">增加</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('Admin/footer')?>
