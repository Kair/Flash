<?php $this->load->view('Admin/header')?>
<script>
function addCheck() {
    var password = $("#password").val();
    var comfirmP = $("#comfirmP").val();
    if (comfirmP != password){
    	alert("两次输入的密码必须一致!");
        $("#password").focus();
        return false;
    }
    return true
}
$(document).ready(function () {
    $("#username").focus();
});
</script>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">新增用户</div>

        <div class="panel-body">

          <form action="<?=base_url('FlashAdmin/save/add_user/user');?>" method="POST" onsubmit="javascript: return addCheck()">
            <input type="text" id="username" name="username" class="form-control" placeholder="用户名" required="required">
            <br>
            <input type="password" id="password" name="password" class="form-control" placeholder="密码" required="required">
            <br>
            <input type="password" id="comfirmP" name="password" class="form-control" placeholder="再次输入" required="required">
            <br>
            <button class="btn btn-lg btn-info">新增用户</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('Admin/footer')?>
