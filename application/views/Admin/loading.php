<?php $this->load->view('Admin/header')?>


<script>
  function timeout() {
    var total = $('#totalSecond').text();
    if(total <= 0) 
    {
      location.href = "<?=base_url('FlashAdmin/view/add_pro')?>";
    } 
    else 
    {
      $('#totalSecond').text(--total);
      window.setTimeout("timeout()", 1000);
    }
  }
  window.setTimeout("timeout()", 1000);
</script>

<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">操作结果</div>

        <div class="panel-body">

          <h1><?=isset($message)?$message:''?></h1><small><span id="totalSecond">3</span>秒后返回。</small>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('Admin/footer')?>
