<?php $this->load->view('Admin/header')?>
<script src="<?= base_url('public/js/bootstrap.file-input.js')?>"></script>
<link href="<?= base_url('public/css/bootstrap.file-input.css')?>" rel="stylesheet">
<script>
$(document).ready(function () {
    $("#name").focus();
});
</script>
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">新增作品</div>
        <?php
        $name=$this->session->userdata('name');
        $author=$this->session->userdata('author');
        $intro=$this->session->userdata('intro');
        ?>
        <div class="panel-body">
          <?php echo form_open_multipart(base_url('FlashAdmin/save_pro'));?>
            <input type="text" id="name" name="name" class="form-control" placeholder="名称" required="required" value="<?=$name?>">
            <br>
            <input type="text" id="author" name="author" class="form-control" placeholder="作者" required="required" value="<?=$author?>">
            <br>
            <textarea id="intro" name="intro" rows="10" class="form-control" required="required" placeholder="简介"><?=$intro?></textarea>
            <br>
            <label><h4>Flash截图：</h4></label>
            <input type="file" title="选择文件" class="span3 custom-file-input" placeholder="标题" id="img" name="img"/>
            <br>
            <label><h4>Flash：</h4></label>
            <input type="file" title="选择文件" class="span3 custom-file-input" placeholder="标题" id="flash" name="flash"/>
            <br>
            <button class="btn btn-lg btn-info">新增</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
<?php
$unsetIt = array('name','author','intro');
$this->session->unset_userdata($unsetIt);
?>
<?php $this->load->view('Admin/footer')?>
