<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Flash</title>
	<link href="<?=base_url('public/css/app.css')?>" rel="stylesheet">
    <script src="<?= base_url('Public/js/jquery-1.11.2.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('Public/js/bootstrap.min.js') ?>" type="text/javascript"></script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=base_url('FlashAdmin')?>"><img src="<?= base_url('public/images/favicon.ico')?>" style="width:20px;height:20px;" alt=""></a>
			</div>
			<?php if ( $this->session->userdata('username') && $this->session->userdata('role') == 'admin'):?>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">管理评委<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?=base_url('FlashAdmin/view/list/user')?>">所有评委</a></li>
							<li><a href="<?=base_url('FlashAdmin/view/add_user')?>">增加评委</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">管理评分标准<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?=base_url('FlashAdmin/view/list/type')?>">所有评分标准</a></li>
							<li><a href="<?=base_url('FlashAdmin/view/add_type')?>">增加评分标准</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">管理作品<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?=base_url('FlashAdmin/view/list/pro')?>">所有作品</a></li>
							<li><a href="<?=base_url('FlashAdmin/view/add_pro')?>">增加作品</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav">
					<li><a href="<?=base_url('FlashAdmin/top')?>">排行</a></li>
				</ul>
				<ul class="nav navbar-nav">
					<li><a href="<?=base_url('')?>">前台首页</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?=$this->session->userdata('username')?><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?=base_url('FlashAdmin/Logout')?>">退出</a></li>
							</ul>
						</li>
				</ul>
			</div>
			<?php endif;?>
		</div>
	</nav>