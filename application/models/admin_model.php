<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/**
 * admin_model
 *
 * 管理员模型
 *
 * @package     Models
 * @author      Kair
 */
class admin_model extends CI_Model
{

    /**
    * get_info_by_field
    * 通过字段获取信息
    *
    * @param  var           $data   条件的值
    * @param  string        $field  字段名
    * @param  string        $table  表名
    * @return   需要的数据
    */
    public function get_info_by_field($data,$field,$table)
    {
        $query = $this->db->get_where($table, array($field => $data));
        return $query->row_array();
    }

    /**
    * add_info
    * 通过字段获取信息
    *
    * @param  $data    array  需要增加的信息
    * @param  $table  string  表名
    * @return   直接返回成功
    */
    public function add_info($data,$table)
    {
        $this->db->insert($table,$data);
        return TRUE;
    }

    /**
    * del_info
    * 删除信息
    *
    * @param    $table      string  表名
    * @param    $condition  array   删除的条件
    * @return   影响的行数
    */
    public function del_info($table,$condition)
    {
        $this->db->delete($table,$condition);
        return $this->db->affected_rows();
    }

    /**
    * get_table
    * 获得表信息
    *
    * @param  $table  string 表名
    * @param  $condition    array or null   获得表的附加条件，可以不写
    * @return   需要的数据
    */
    public function get_table($table,$condition=null)
    {
        if ($condition == null)
            return $this->db->get($table)->result_array();
        else
            return $this->db->get_where($table,$condition)->result_array();
    }
    
    /**
    * is_exist
    * 查询信息是否存在
    *
    * @param     $table  string    表名
    * @param     $condition   array    附加条件
    * @return    存在返回TRUE，否则FALSE
    */
    public function is_exist($table,$condition)
    {
        if ($this->db->get_where($table, $condition)->num_rows>0)
            return TRUE;
        return FALSE;
    }

    /**
    * update_field
    * 更新字段
    *
    * @param     $table  string    表名
    * @param     $data   array   需要设置的信息
    * @param     $condition   array    附加条件
    * @return    影响的行数
    */
    public function update_field($table,$data,$condition)
    {
        $this->db->set($data);
        $this->db->where($condition);
        $this->db->update($table);
        return $this->db->affected_rows();
    }

    /**
    * get_top
    * 获得排行榜
    *
    * @return   评分的排行
    */
    public function get_top()
    {
        $this->db->order_by("score", "desc");
        return $this->db->get('pro')->result_array();
    }
}
